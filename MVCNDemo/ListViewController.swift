//
//  ListViewController.swift
//  MVCNDemo
//
//  Created by Andriy Hanchak on 11/9/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {

    // MARK: Properties
    
    let spinner = UIActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: 48, height: 48)))
    var listModel: NetworkModel?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bounds = UIScreen.mainScreen().bounds
        
        spinner.backgroundColor = UIColor(colorLiteralRed: 127/255, green: 127/255, blue: 127/255, alpha: 0.5)
        spinner.center = CGPoint(x: bounds.width / 2.0, y: bounds.height / 2.0 - 16)
        spinner.layer.cornerRadius = 5
        spinner.hidesWhenStopped = true
        
        view.addSubview(spinner)
        
        clearsSelectionOnViewWillAppear = false
        tableView.tableFooterView = UIView(frame: .zero)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        spinner.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
    }
    
    // MARK: - Private Methods
    
    func dataSourceDidChange(success: Bool) {
        spinner.stopAnimating()
        tableView.reloadData()
    }
}
