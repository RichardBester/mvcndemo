//
//  CommentListModel.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/8/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class CommentListModel<T: Comment>: ListModel<T> {

	override var path: String {
		return "/1/classes/Comment"
	}

	required init() {
		super.init()
	}

	func setupQueryForMessage(messageId: String) {
		query = "include=author&where%5B%24relatedTo%5D%5Bkey%5D=comments&where%5B%24relatedTo%5D%5Bobject%5D%5BclassName%5D=Message&where%5B%24relatedTo%5D%5Bobject%5D%5B__type%5D=Pointer&where%5B%24relatedTo%5D%5Bobject%5D%5BobjectId%5D=\(messageId)"
	}

}
