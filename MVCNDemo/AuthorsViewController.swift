//
//  AuthorsViewController.swift
//  MVCNDemo
//
//  Created by Andriy Hanchak on 11/9/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

protocol AuthorsViewControllerDelegate {
    func didSelectAuthor(author: Author)
}

class AuthorsViewController: ListViewController {
    
    // MARK: - Properties
    
    var delegate: AuthorsViewControllerDelegate?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listModel = AuthorListModel()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        (listModel as? AuthorListModel)?.load(dataSourceDidChange)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = listModel as? AuthorListModel {
            return model.results.count
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ThreeLinesCell", forIndexPath: indexPath)
        
        if let threeLinesCell = cell as? ThreeLinesCell, model = listModel as? AuthorListModel {
            let index = indexPath.row
            let author = model.results[index]
            
            if let firstName = author.firstName {
                threeLinesCell.line1Label.text = "First Name: \(firstName)"
            }
            
            if let lastName = author.lastName {
                threeLinesCell.line2Label.text = "Last Name: \(lastName)"
            }
            
            if let birthDate = author.birthDate {
                threeLinesCell.line3Label.text = "Birth Date: \(birthDate)"
            }
            
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let model = listModel as? AuthorListModel {
            let index = indexPath.row
            let author = model.results[index]
            
            delegate?.didSelectAuthor(author)
            navigationController?.popViewControllerAnimated(true)
        }
    }
}
