//
//  ThreeLinesCell.swift
//  MVCNDemo
//
//  Created by Andriy Hanchak on 11/9/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class ThreeLinesCell: UITableViewCell {

    @IBOutlet weak var line1Label: UILabel!
    @IBOutlet weak var line2Label: UILabel!
    @IBOutlet weak var line3Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
