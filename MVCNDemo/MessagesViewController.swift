//
//  MessagesViewController.swift
//  MVCNDemo
//
//  Created by Andriy Hanchak on 11/9/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class MessagesViewController: ListViewController {
    
    // MARK: - Lifecycle
    
    private var message: Message?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listModel = MessageListModel()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        (listModel as? MessageListModel)?.load(dataSourceDidChange)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "to-comments" {
            (segue.destinationViewController as? CommentsViewController)?.message = message
        }
    }

    // MARK: - UITableViewDataSource

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = listModel as? MessageListModel {
            return model.results.count
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ThreeLinesCell", forIndexPath: indexPath)
        
        if let threeLinesCell = cell as? ThreeLinesCell, model = listModel as? MessageListModel {
            let index = indexPath.row
            let message = model.results[index]
            
            if let firstName = message.author?.firstName, lastName = message.author?.lastName {
                threeLinesCell.line1Label.text = "Author: \(firstName) \(lastName)"
            }
            
            if let title = message.title {
                threeLinesCell.line2Label.text = "Title: \(title)"
            }
            
            if let message = message.text {
                threeLinesCell.line3Label.text = "Message: \(message)"
            }

        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let model = listModel as? MessageListModel {
            let index = indexPath.row
            let message = model.results[index]
            
            self.message = message
            
            performSegueWithIdentifier("to-comments", sender: self)
        }
    }
}
