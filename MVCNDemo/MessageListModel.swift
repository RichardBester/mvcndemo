//
//  MessageListModel.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/8/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class MessageListModel<T: Message>: ListModel<T> {

	override var path: String {
		return "/1/classes/Message?include=author"
	}

	required init() {
		super.init()
	}
}
