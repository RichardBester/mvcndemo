//
//  Author.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/7/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import Foundation

class Author: NetworkModel {

	override var path: String {
		return "/1/classes/Author"
	}

	var firstName: String?
	var lastName: String?
	var birthDate: NSDate?

	override func setValue(value: AnyObject?, forKey key: String) {
		if key == "birthDate" {
			if let stringDate = (value as? [String: String])?["iso"] {
				birthDate = NetworkModel.ISO8601DateFormatter.dateFromString(stringDate)
			}
		} else {
			super.setValue(value, forKey: key)
		}
	}

	func create(completion: SuccessfulRequestHandler? = nil) {
		performRequest({ [unowned self] (builder) -> Void in
			let date = ["__type": "Date","iso": NetworkModel.ISO8601DateFormatter.stringFromDate(self.birthDate!)]
			let JSON = ["firstName": self.firstName!, "lastName": self.lastName!, "birthDate": date]
			builder.path = self.path
			builder.method = .POST
			builder.body = .JSON(JSON: JSON)
		}) { [unowned self] (success) -> Void in
			print("Author has been created: \(self.objectId), \(self.createdAt)")
			completion?(success: success)
		}
	}

	func load(id: String, completion: SuccessfulRequestHandler? = nil) {
		performRequest({ (builder) -> Void in
			builder.path = self.path + "/\(id)"
			builder.method = .GET
		}) { (success) -> Void in
			print("Author has been loaded: \(self.objectId), \(self.createdAt)")
			completion?(success: success)
		}
	}
}