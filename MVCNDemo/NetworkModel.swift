//
//  NetworkModel.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/7/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import Foundation

class NetworkModel: NSObject {

	var path: String {
		return ""
	}

	enum Error: ErrorType {
		case NilData
		case ServerError
	}

	required override init() {
		super.init()
	}

	static let defaultHeaders = ["X-Parse-Application-Id": "fpNX7HmL52EPU19B39mj3HKnGFEleH2bAhKw68A9", "X-Parse-REST-API-Key": "GCzfgyRCRI9UKMdd7Un0Whb5jioomJdtU1PKqQZc", "Content-Type": "application/json"]

	static let baseURL = NSURL(string: "https://api.parse.com/")!

	static let undefinedKey = "JSON Statham"

	typealias SuccessfulRequestHandler = (success: Bool) -> Void
	typealias FailedRequestHandler = (model: NetworkModel, error: ErrorType?) -> Void

	var failureHandler: FailedRequestHandler?

	var objectId: String?
	var createdAt: NSDate?
	var updatedAt: NSDate?

	func performRequest(request: NSURLRequest, completion: SuccessfulRequestHandler? = nil) {
		let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { [unowned self] (data, response, error) -> Void in
			dispatch_async(dispatch_get_main_queue()) { () -> Void in
				guard let HTTPResponse = response as? NSHTTPURLResponse else {
					self.failureHandler?(model: self, error: error)
					return
				}
				guard error == nil else {
					self.failureHandler?(model: self, error: error)
					return
				}
				guard let data = data else {
					self.failureHandler?(model: self, error: Error.NilData)
					return
				}

				if 0...299 ~= HTTPResponse.statusCode {
					let JSONObject = try? NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
					if let dict = JSONObject as? [String: AnyObject] {
						self.setValuesForKeysWithDictionary(dict)
						completion?(success: true)
					} else if let arr = JSONObject as? [AnyObject] {
						self.setValue(arr, forUndefinedKey: NetworkModel.undefinedKey)
						completion?(success: true)
					} else {
						completion?(success: false)
					}

				} else {
					self.failureHandler?(model: self, error: Error.ServerError)
				}
			}
		}
		task.resume()
	}

	func performRequest(buildingClosure: (builder: URLRequestBuilder) -> Void, completion: SuccessfulRequestHandler? = nil) {
		let builder = URLRequestBuilder(baseURL: NetworkModel.baseURL)
		builder.headerFields = NetworkModel.defaultHeaders
		buildingClosure(builder: builder)
		if let request = builder.URLRequest {
			performRequest(request, completion: completion)
		}
	}

	override func setValue(value: AnyObject?, forKey key: String) {
		guard !(value is NSNull) else {
			return
		}
		switch key {
			case "createdAt":
				if let stringDate = value as? String {
					createdAt = NetworkModel.ISO8601DateFormatter.dateFromString(stringDate)
				}
			case "updatedAt":
				if let stringDate = value as? String {
					updatedAt = NetworkModel.ISO8601DateFormatter.dateFromString(stringDate)
				}
			default:
				super.setValue(value, forKey: key)
		}
	}

	static let ISO8601DateFormatter: NSDateFormatter = {
		let formatter = NSDateFormatter()
		formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"//"YYYY-MM-dd'T'HH:mm:ssZZZ"
		return formatter
	}()

}