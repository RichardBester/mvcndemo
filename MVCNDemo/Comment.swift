//
//  Comment.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/7/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class Comment: NetworkModel {

	override var path: String {
		return  "/1/classes/Comment"
	}

	var text: String?
	var author: Author?

	override func setValue(value: AnyObject?, forKey key: String) {
		if key == "author" {
			if let dict = value as? [String: AnyObject] {
				author = Author()
				let keys = ["objectId", "createdAt", "updatedAt", "firstName", "lastName", "birthDate"]
				for key in keys {
					author?.setValue(dict[key], forKey: key)
				}
			}
		} else {
			super.setValue(value, forKey: key)
		}
	}

	func create(completion: SuccessfulRequestHandler? = nil) {
		author?.create({ [unowned self] (success) -> Void in
			self.performRequest({ (builder) -> Void in
				var JSON: [String: AnyObject] = ["text": self.text!]
				if let authorID = self.author?.objectId {
					JSON["author"] = ["__type":"Pointer","className":"Author","objectId": authorID]
				}
				builder.path = self.path
				builder.method = .POST
				builder.body = .JSON(JSON: JSON)
			}, completion: { (success) -> Void in
				print("Comment has been created: \(self.objectId), \(self.createdAt)")
				completion?(success: success)
			})
		})
	}

	func load(id: String, completion: SuccessfulRequestHandler? = nil) {
		performRequest({ (builder) -> Void in
			builder.path = self.path + "/\(id)?include=author"
			builder.method = .GET
		}) { (success) -> Void in
			print("Comment has been loaded: \(self.objectId), \(self.createdAt)")
			completion?(success: success)
		}
	}

}
