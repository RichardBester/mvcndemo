//
//  CommentsViewController.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/7/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class CommentsViewController: ListViewController {

    // MARK: - Properties
    
    var message: Message?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listModel = CommentListModel()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let messageId = message?.objectId {
            (listModel as? CommentListModel)?.failureHandler = failedLoadComments
            (listModel as? CommentListModel)?.setupQueryForMessage(messageId)
            (listModel as? CommentListModel)?.load(dataSourceDidChange)
        } else {
            print("no message id...")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = listModel as? CommentListModel {
            return model.results.count
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ThreeLinesCell", forIndexPath: indexPath)
        
        if let threeLinesCell = cell as? ThreeLinesCell, model = listModel as? CommentListModel {
            let index = indexPath.row
            let comment = model.results[index]
            
            if let firstName = comment.author?.firstName {
                threeLinesCell.line1Label.text = "First Name: \(firstName)"
            }
            
            if let lastName = comment.author?.lastName {
                threeLinesCell.line2Label.text = "Last Name: \(lastName)"
            }
            
            if let message = comment.text {
                threeLinesCell.line3Label.text = "Message: \(message)"
            }
            
        }
        
        return cell
    }
    
    // MARK: - Private
    
    private func failedLoadComments(model: NetworkModel, error: ErrorType?) {
        print("error: \(error)")
    }
}

