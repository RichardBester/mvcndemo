//
//  Message.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/7/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class Message: NetworkModel {

	override var path: String {
		return  "/1/classes/Message"
	}

	var title: String?
	var text: String?
	var rating: NSNumber?
	var author: Author?
	var comments: [Comment]?

	override func setValue(value: AnyObject?, forKey key: String) {
		if key == "author" {
			if let dict = value as? [String: AnyObject] {
				author = Author()
				let keys = ["objectId", "createdAt", "updatedAt", "firstName", "lastName", "birthDate"]
				for key in keys {
					author?.setValue(dict[key], forKey: key)
				}
			}
		} else {
			super.setValue(value, forKey: key)
		}
	}

	func create(completion: SuccessfulRequestHandler? = nil) {
		guard let title = title, text = text, rating = rating, authorId = author?.objectId else {
			completion?(success: false)
			return
		}
		self.performRequest({ (builder) -> Void in
			var JSON: [String: AnyObject] = ["text": text, "title": title, "rating": rating]
			JSON["author"] = ["__type":"Pointer","className":"Author","objectId": authorId]
			builder.path = self.path
			builder.method = .POST
			builder.body = .JSON(JSON: JSON)
		}, completion: { (success) -> Void in
			print("Message has been created: \(self.objectId), \(self.createdAt)")
			completion?(success: success)
		})
	}

	func addComment(comment: Comment, completion: SuccessfulRequestHandler? = nil) {
		comments?.append(comment)
		let JSON = ["comments": ["__op": "AddRelation","objects":[["__type":"Pointer","className":"Comment","objectId":"\(comment.objectId!)"]]]]
		performRequest({ [unowned self] (builder) -> Void in
			builder.path = self.path + "/\(self.objectId!)"
			builder.method = .PUT
			builder.body = .JSON(JSON: JSON)
		}, completion: completion)
	}

}
