//
//  AuthorListModel.swift
//  MVCNDemo
//
//  Created by Sauron Black on 11/8/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class AuthorListModel<T: Author>: ListModel<T> {

	override var path: String {
		return "/1/classes/Author"
	}

	required init() {
		super.init()
	}
}
