//
//  ListModel.swift
//  iTacit
//
//  Created by Sauron Black on 11/2/15.
//  Copyright © 2015 iTacit. All rights reserved.
//

import Foundation

class ListModel<Element: NetworkModel>: NetworkModel {

	private(set) var results = [Element]()

	subscript(index: Int) -> Element {
		get {
			return results[index]
		}
		set {
			results[index] = newValue
		}
	}

	var query: String?

	required init() {
		super.init()
	}

	var count: Int {
		return results.count
	}

	func load(completion: SuccessfulRequestHandler? = nil) {
		performRequest({ [unowned self] (builder) -> Void in
			if let query = self.query {
				builder.path = self.path + "?" + query
			} else {
				builder.path = self.path
			}
			builder.method = .GET
		}, completion: completion)
	}

	func clear() {
		results = []
	}

	override func setValue(value: AnyObject?, forKey key: String) {
		if key == "results" {
			if let arr = value as? [[String: AnyObject]] {
				results = arr.map { (dict) -> Element in
					let element = Element()
					element.setValuesForKeysWithDictionary(dict)
					return element
				}
			}
		} else {
			super.setValue(value, forKey: key)
		}
	}

}
