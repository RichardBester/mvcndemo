//
//  NewMessageViewController.swift
//  MVCNDemo
//
//  Created by Andriy Hanchak on 11/9/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import UIKit

class NewMessageViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var authorButton: UIButton!
    
    private var author: Author?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "to-authors" {
            (segue.destinationViewController as? AuthorsViewController)?.delegate = self
        }
    }
  
    @IBAction func sendAction(sender: AnyObject) {
        if let _ = author {
            let message = Message()
            message.title = titleTextField.text
            message.text = messageTextField.text
            message.author = author
            message.rating = 5
        
            message.create(messageSent)
        } else {
            print("select author...")
        }
    }
    
    private func messageSent(success: Bool) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}

extension NewMessageViewController: AuthorsViewControllerDelegate {
    
    func didSelectAuthor(author: Author) {
        self.author = author
        
        if let firstName = author.firstName, lastName = author.lastName {
            authorButton.setTitle("\(firstName) \(lastName)", forState: .Normal)
        }
    }
}
