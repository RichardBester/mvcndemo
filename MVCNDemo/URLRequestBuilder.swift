//
//  RequestBuilder.swift
//  MVCNTest
//
//  Created by Sauron Black on 10/30/15.
//  Copyright © 2015 Mordor. All rights reserved.
//

import Foundation

class URLRequestBuilder {

	var baseURL: NSURL
	var path: String?
	var method: URLRequestMethod?
	var headerFields = [String: String]()
	var body: URLRequestBody?

	init(baseURL: NSURL) {
		self.baseURL = baseURL
	}

	var URLRequest: NSURLRequest? {
		guard let path = path, URL = NSURL(string: path, relativeToURL: baseURL), method = method else {
			return nil
		}

		let request = NSMutableURLRequest(URL: URL)
		request.HTTPMethod = method.rawValue

		if !headerFields.isEmpty {
			request.allHTTPHeaderFields = headerFields
		}


		if let data = body?.data {
			request.setValue("\(data.length)", forHTTPHeaderField: "Content-Length")
			request.HTTPBody = data
		}

		return request
	}
}

enum URLRequestMethod: String {

	case GET
	case POST
	case PUT
	case DELETE
	case HEAD
	case PATCH
	case OPTIONS

}

enum URLRequestBody {

	case RawData(data: NSData)
	case JSON(JSON: AnyObject)

	var data: NSData? {
		switch self {
			case .RawData(let data):
				return data
			case .JSON(let JSON):
				return  try? NSJSONSerialization.dataWithJSONObject(JSON, options: .PrettyPrinted)
		}

	}

}
